<?php
// =============================================================================
// TEMPLATE NAME: 404
// -----------------------------------------------------------------------------
// 404 Page Not Found template. This template is hidden.
// =============================================================================
$logo = ALPINS_URL . "/inc/logo.png";
get_header();
if (defined("HC_PLUGIN_PATH")) {
    $tmp = hc_get_setting("logo");
    if ($tmp != "") $logo = $tmp;
}
?>
<div class="section-base">
    <div class="container">
        <div class="row align-items-center full-screen">
            <div class="col-md-12 align-center">
                <h1>
                    <?php esc_html_e("404","alpins") ?>
                </h1>
                <h3>
                    <?php esc_html_e("PAGE NOT FOUND","alpins") ?>
                </h3>
                <p>
                    <?php esc_html_e("The page you were looking for can not be found.","alpins") ?>
                </p>
                <hr class="space m" />
                <a class="btn-sm btn" href="<?php echo esc_url(home_url()) ?>">
                    <?php esc_html_e("Go back to home","alpins") ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>