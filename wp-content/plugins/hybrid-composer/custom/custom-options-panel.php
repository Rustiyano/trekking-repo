<?php

/*
=============================================================================
CUSTOM SETTINGS FOR THE THEME OPTIONS PANEL
=============================================================================

Add new settings to the theme options panel here.
Every array item is a new setting.

Available values for "type" setting: checkbox,select,text,textarea,color,image_upload
Available values for "label" setting: main,layout,menu,footer,lists,titles,customizations,social

$HC_CUSTOM_PANEL
name : Theme's name
version : Theme's version
colors : Theme's panel colors

Documentation: wordpress.framework-y.com/advanced-api-documentation/#custom-theme

 */

global $HC_CUSTOM_PANEL;

$HC_CUSTOM_PANEL = array(
	'name'    => 'Alpins',
	'version' => '1.4.7',
    'colors'  => array("#404040","#272727"),
    'demos' => array(array('id' => 'alpins','name' => 'Main')),
    'demos_url' => 'http://themes.framework-y.com/demo-import/'
);


$HC_CUSTOM_FONT = "Montserrat:500,600,700,800";
$HC_SITE_FONTS = 'body, textarea {
    font-family: [FONT-1] !important;
}';


$HC_SITE_COLORS = 'body, header h2, .recentcomments a, h1, h2, h3, h4, h5, h6, .input-text, .input-select, .input-textarea, .icon-list span, .icon-list span a, .social-links a i, .glide__arrow, .search-bar input[type=submit], .menu-inner li > a, .accordion-list > li > a, .cnt-box-blog-side .icon-list a, .cnt-box-blog-top .icon-list a, .list-nav a, .media-box-down .caption h2, .media-box-down .caption p {
    color: [COLOR-4];
}

div.cnt-box-info .cnt-info, footer, .full-slider .media-box-down .caption,.grid-item:not(:first-child) .cnt-box-top-icon .extra-field,.custom-lightbox,.section-image.light .boxed-area, .cnt-box-testimonials-bubble > p, [class*=box-lightbox].light, .datepicker-panel > ul > li, .datepicker-panel > ul[data-view="week"] > li, .datepicker-panel > ul[data-view="week"] > li:hover, .datepicker-top-left, .datepicker-top-right, .datepicker-panel > ul > li.disabled, .datepicker-panel > ul > li.disabled:hover, .cnt-box-blog-side .blog-date span:last-child, .cnt-box-blog-top .blog-date span:last-child, .section-home-alpins + section:before, .scroll-menu .shop-menu-cnt .cart-count {
    background-color: [COLOR-4];
}

.cnt-box-testimonials-bubble > p:after {
    border-right-color: [COLOR-4];
}

.tab-nav li.active a, .tab-nav li:hover a {
    background-color: [COLOR-4];
    border-color: [COLOR-4];
}

.lan-menu .dropdown > ul > li > a,.custom-lightbox h3,.cnt-box-top-icon .extra-field,.mfp-title,.cnt-box-side-icon .extra-field, body > nav.scroll-menu:before, .title > p:before, .title > p:after, .quote .quote-author:before, .cnt-box-info .extra-field, .glide__bullets > button:hover, .glide__bullets > button.glide__bullet--active, .controls-right .glide__bullet, .media-box-reveal .extra-field, .media-box .caption h3:before, .bg-color, body main > section.bg-color, nav .dropdown > ul > li > a, body > header h2:after, body > header h2:before, .controls-out.arrows-left .glide__arrow:hover, .menu-cnt > ul > li > ul li > a, .album-box .caption h3:after, .menu-inner li:before, .cnt-box-blog-side .blog-date, .cnt-box-blog-top .blog-date, .section-home-alpins .glide__arrow, .menu-inner:not(.menu-inner-vertical) .dropdown ul > li > a, .step-item:before, .step-item > span, .shop-menu-cnt .cart-count {
    background-color: [MAIN-COLOR];
}

.section-home-alpins .glide__bullets .glide__bullet.glide__bullet--active,.light .bottom-left-nav .glide__bullets > button.glide__bullet--active {
    background-color: [MAIN-COLOR] !important;
}

@media (max-width: 991.98px) {
    nav.scroll-menu .menu-cnt {
        background-color: [MAIN-COLOR];
    }
}

.btn, .icon-links-grid a:hover, .icon-links-button a {
    background-color: [MAIN-COLOR];
    border-color: [MAIN-COLOR];
}

a, .counter .value span:last-child, .icon-box i,.box-sign b, .cnt-pricing-table > ul > li:before, .cnt-pricing-table .price label, .cnt-box-info .cnt-info > div > span:last-child, .cnt-box-info .bottom-info, .btn-text, .menu-cnt > ul > li:hover > a, nav:not(.menu-transparent) .menu-cnt > ul > li:hover > a, .lan-menu > li:hover > a, nav:not(.menu-transparent) .lan-menu > li:hover > a, .timeline > div:hover .badge p, .cnt-box-team:hover .caption h2, .cnt-box-top-icon > i, .datepicker-panel > ul > li:hover, .extra-field:before, .menu-inner li.active > a, .menu-inner li:hover > a, .table-alpins td:first-child, .accordion-list > li > a:before, .cnt-box-blog-side .icon-list i, .cnt-box-blog-top .icon-list i, .list-nav a:before, .cnt-box .caption p b, .cnt-box-side-icon > i, .countdown h3, .countdown p, nav:not(.menu-transparent) .menu-mini .lan-menu > li:hover > a, nav .icon-list li > i {
    color: [MAIN-COLOR];
}

.breadcrumb li a:hover, .light .breadcrumb li:not(:last-child) a:hover, .light .text-color, .text-color, .menu-inner .dropdown li:hover > a, .media-box.media-box-down h3 {
    color: [MAIN-COLOR] !important;
}

form.form-box textarea:focus, form.form-box select:focus, form.form-box input:focus, [data-parallax] .table td, .media-box-half .caption .extra-field + p, .timeline > div .panel, div.timeline > div.inverted .panel:last-child, .search-box-menu > input[type=text]:focus {
    border-color: [MAIN-COLOR];
}

.lan-menu .dropdown > ul > li:hover > a, .btn:not(.btn-border):hover, .dropdown ul:not(.icon-list) li:hover > a,  .lan-menu > li:hover > a, .icon-links-button a:hover, .pagination li.page:hover a, .pagination li.page.active a, .section-home-alpins .glide__arrow:hover, .menu-inner:not(.menu-inner-vertical) .dropdown ul > li:hover > a {
    background-color: [HOVER-COLOR];
}

.btn-text:hover, .accordion-list > li > a:hover, .list-nav a:hover,.menu-transparent:not(.scroll-menu) .menu-cnt > ul > li:hover > a  {
    color: [HOVER-COLOR];
}

@media (max-width: 991.98px) {
    nav.scroll-menu .dropdown > ul > li > a {
        background-color: [HOVER-COLOR];
    }

    .menu-transparent:not(.scroll-menu) .menu-cnt > ul > li.dropdown.active > a {
        color: [HOVER-COLOR] !important;
    }
}

.icon-box p, .cnt-call .caption p, .cnt-box-team .caption span, .cnt-box .caption p, .cnt-pricing-table > ul > li, .timeline .panel p {
    color: [COLOR-3];
}';
?>
